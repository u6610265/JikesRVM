<h1>Modified File</h1>
<p>This file list the files and exact place and that I modified:</p>
<ul>
<li><p><b>MemoryManager.java:</b></p>
<p>99-120: Declaring added attributes</p>
<p>501-504: in method pickAllocatorForType()</p>
<p>874-1165: in method newTIB()</p></li>

<li><p><b>AlignmentEncoding.java:</b></p>
<p>54: Change the value of attributes</p>
</li>

<li><p><b>HandInlinedScanning.java:</b></p>
<p>30-38: Change the value of attributes</p>
</li>

<li><p><b>Plan.java:</b></p>
<p>89,116,137: Declaring added attributes</p></li>   

<li><p><b>MutatorContext.java:</b></p>
<p>116: Declaring added attributes</p>
<p>196: in method alloc()</p>
<p>221: in method postAlloc()</p>
<p>248: in method getAllocatorFromSpace()</li>      

<li><p><b>TraceLocal.java:</b></p>
<p>286-287: in method traceObject()</p></li>   

<li><p><b>BootImageWriter.java:</b></p>
<p>154-159: Declaring added attributes</p>
<p>717-808: in main function</p>
<p>1855-1865: in method copyToBootImage()</p></li>

<li><p><b>TIB.java:</b></p>
<p>65-82: Declaring added attributes and the method to get and set them</p></li>      

<li><p><b>BootImage.java:</b></p>
<p>81: Declaring added attributes</p>
<p>220: in method getDataSize()</p>
<p>302-329: change the method allocateDataStorage()</p>
<p>367: in method resetAllocator()</p></li>

<li><p><b>ObjectModel.java:</b></p>
<p>139-142: Declaring added attributes</p>
<p>1011-1029: in method allocateArray()</p></li>
