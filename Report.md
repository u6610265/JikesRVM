<h1> Generalization and Optimization of Type Inofrmation Block(TIB) Allocator in Jikes RVM</h1>

<h2>Acknowledgements</h2>
<p>This is the report of course COMP 3710 in Australian National Univeristy enrolled by Yicheng Wang(u6610265)
   in Semester 1, 2018. I would like to express my sincere gratitude to my supervisor Prof Stephen Blackburn for his help, advice, and 
   support throughout the semester. I would also express my gratitude to my Examiner
   Professor Tony Hosking, my course convenor Professor Peter Strazdins, and Dr Ramesh Sankaranarayana
   for the support to my project and the tolerance of my ignorance of the course process.
   I would also like to thank all the members in our MMTK research group for their kind
   help. In addition, I want to thanks Australian National Univeristy and University of Chinese Academy of Sciences
   for giving me this chance to exchange and study. 
</p>
<h2>Abstract</h2>
<p><a href="http://www.jikesrvm.org">Jikes RVM</a> is a Research Virtual Machine provides 
    a flexible open testbed to prototype virtual machine technologies and experiment with a 
    large variety of design alternatives. According to the idea in the paper
    <a href="http://users.cecs.anu.edu.au/~steveb/downloads/pdf/scan-ismm-2011.pdf">A Comprehensive Evaluation of Object Scanning Techniques</a>[1],
     Jikes RVM <i>"selectively aligning the TIB (vtable) of each class"</i> so that it could <i>"effectively encode metadata into the header field that stores the
     TIB pointer."</i> However, with the former implementation in Jikes RVM, it will waste a lot space for padding when
     generalize more bits for encoding. What I have done is to generalize and optimize the TIB allocator in Jikes RVM, both
     at the run time and the build time(boot image time). This report will describe the approaches I reduce the wasted
     space, and illustrate the result of each approach. Finally, I will point out the parts that need to be improved 
     and share my opinion.
<h2>Introduction</h2>

<p>Jikes RVM now use 3 bits of the address of TIBs to encode for Garbage Collection(GC) metadata, the approach to settle every TIB in an appropriate
   Address is <i>"when allocating a TIB and encoding n bits of metadata, we allocate a block of memory 2<sup>3</sup> words
   larger than the TIB itself."</i>  We could generalize 3 bits of the address to more bits for encoding, each bit or each group of bits
   stands for the value of a immutable state, which will not be changed since a class is built. In this way, we don need
   to call the function for the value of this state and "obtain these metadata bits for ‘free’".
</p>
<table>
<tr>
<td align="center"><img src="Generalization.JPG" alt="Generalization"></td>
</tr>
<tr>
<td align="center"><b>Figure 1. </b>The former allocator and the generalization (2 bits required to naturally align a pointer in a 32-bit machine)</td>
</tr>
</table>   
<p>This approach will not waste too much space when only 3 bits are used, because the size of TIBs themselves is the 
   main cost of the space. However, it will be wasteful when the number of bits to be encoded is more. According to the
   measurements in various benchmarks, the mean size of a TIB is approximately 160 bytes, for 3 bits, it will waste
   32 bytes each, which is not a big deal. As for 8 bits, it will waste 1024 bytes, which is unbearable.</p>
<p>Therefore, we need to optimize the TIB allocator to reduce waste if we want to generalize. For the reason that  
   Jikes RVM is implemented in the Java™ programming language and is self-hosted, there are two cases
   that TIBs will be allocated:</p>
<ul>
<li>Build time:The TIBs created by JDK when compile Jikes RVM. These TIBS are stored in the Jikes RVM 
    Table Of Contents(JTOC), the bootimage writer need to allocate spaces in the bootimage and then copy them to there.</li>
<li>Run time: When running a JAVA project, TIBs of new classes in the project need to be allocated and settled 
    in the memory.</li>
</ul>
<p> At the build time, for every time it builds is the same, we could know all the TIBs and their basic information(e.g. required align code, size),   
    which will never changed. Thus we can global optimize the allocator, to arrange them in a proper order and space
    that can waste the least space.
</p>
<p> At the run time, we will never know how many TIBs will be created when running a project, also the TIBs come
    to memory manager one by one and needed to be settled at once. Thus what we can do is do the optimize dynamically,
    put the proper TIBs on the holes in the padding area of the previous TIBs.
</p>
<p> The Optimizations above all work fine, which finally reduce more than half of the waste space. However,
    what immutable states will be used for these expanded bits is the work of my classmate and have not be 
    decided yet. Once these bits used, there are some parts of my work need to be revised. Also there are some 
    approaches to improve the optimization, I will mention them at the end of the report.
</p>
<p>The list of files and codes that I changed is in <a href="Modified_File.md">Modified_File.md</a>, which is in the root directory 
   of this project.</p>
<h2>Methodology</h2>
<p> In order to implement the optimization method more conveniently, I separate the space for TIB and the space for
    normal data both at the build time and the run time.
</p>
<p> For the build time, the start of the boot image in the heap is at the address <i>0x60000000</i>, the data other than TIBs start from this address and will extend until
    approximately <i>0x61d45000</i>. Hence I settle the TIB space start from <i>0x62000000</i>. I use <i>freeTIBOffset</i>
    as the cursor of the TIB space which presents the offset from the <i>BOOT_IMAGE_DATA_START</i> <i>(0x60000000)</i>.
    The <i>BOOT_IMAGE_DATA</i> is limited at <i>0x63400000</i>, it will never exceed this limitation. I let the size of
    <i>BOOT_IMAGE_DATA</i> calculated by the <i>freeTIBOffset</i>, so that both two parts will be included in boot image data.
</p>    
<p> For the run time, I create a ImmortalSpace named <i>immortalTIBSpace</i> and deal with the correlative code
    just like other space in the plan. Then I create a bump pointer allocator named <i>immortalTIB</i>, which contains 
    the cursor of free TIB space. Every time before it allocates a new TIB in the memory manager at the run time, it will
    set the allocator as <i>immortalTIB</i> and allocate space in the <i>immortalTIBSpace</i>.
</p>
<p> Generalization is not a big task base on the former structure of Jikes RVM. All I need to change is the value of
    <i>AlignmentEncoding.<b>FIELD_WIDTH</b></i>, which stands for the bits we use to encode. The major work is optimization.
<ul>
<li><h3>Remove the tail<h3>
<p> As mentioned below, the former allocator of Jikes RVM allocates a block of memory 2<sup>n</sup> words larger 
    than the TIB itself. Then adjusts the region in this space to find a address fit the align code and set the TIB
    there, which confirms there is a proper address in the allocated space. Nevertheless, we can note that the <i>tail</i> of the allocated space is never be used. It is not worth being noticed
    when n=3, but it is a significant number when n is bigger. We know the size of the TIB and the cursor of the 
    free TIB space, thus we can definitely calculate the end of TIB after adjustment and get how much space we
    need exactly. In this way, we can avoid the allocation of the <i>tail</i>. It is a simple trick but works
    effectively both at run time and build time.
</p>
<table>
<tr>
<td align="center"><img src="Tail.JPG" alt="Tail"></td>
</tr>
<tr>
<td align="center"><b>Figure 2. </b>Reduce the tail of allocated space</td>
</tr>
</table>   
</li>

<li><h3>Greedy Algorithm</h3>
<p> Indeed we can collocet the information of all TIBs at build time, However, the former structure of bootimage writer goes through
    the objects as well as TIBs in JTOC one by one, then allocate and copy each of them into the boot image, it works just like run time.
    So we need to scan all the TIBs and arrange their location before they are copied and set. I do this work in the main function of <i>BootImageWriter</i>, then divided them into eight groups according to 
    their align code(3 bits for encoding, totally 8 cases) and store them in the array. After that, we allocate space 
    for all these TIBs by greedy algorithm:
</p>
<p> We know the cursor of free TIB space, hence we can calculate which group of TIB will have the least padding. 
    Then we pick one TIB from this group, the principle to choose in the group is also greedy
    algorithm, after the TIB we choose, the remaining members of the group we choose for the next TIB should be the 
    most (We want to allocate the TIB in the group which have more members first). Because the total size of all the TIBs 
    is fixed, the less the padding is, the less the waste is, greedy algorithm minimize the padding.
</p> 
<p> In this way, we allocate a space in the TIB spae of boot image for every TIB at build time. We store the Address
    of each TIB in itself. When an object is scanned and copied to boot image, we can judge whether it is a TIB by the 
    alignment it need. If it is a TIB, we don not have to allocate again, just extract its address, and set it there.
    If not, we allocate a space in the data space of boot image normally.
</p> 
<table>
<tr>
<td align="center"><img src="Greedy.JPG" alt="Greedy"></td>
</tr>
<tr>
<td align="center"><b>Figure 3. </b>Use greedy algorithm to reduce the padding</td>
</tr>
</table>   
</li>

<li><h3>Fill the holes</h3>
<p> When the number of bits we encode is less than 7, the mainly cost of the memory is the TIB itself, the padding area is mostly smaller
    than the size of TIB. But when the number of bits is more than 7, the padding area of one TIB may big enough to set another TIB.
    Therefore, we can store the location of these holes which are produced by region adjustment. If there is any TIB fit the Address in these 
    area and once we put it there, it will not cover other TIBs we already set down, we can use this TIB to fill this hole.
</p>
<p> The way I implement is finding out all the appropriate addresses in the padding area, if the space for these addresses is big enough,
    add each address to the corressponding list created for storage. When allocating the TIB after, firstly check if there is any member in its 
    corressponding list, then check whether the size of this TIB is smaller than the size limitation. If both of the requirements is
    satisfied, then we can give remove this address from the list, give it to the TIB as the start address.
</p>
<p> As for the size limitation, it should be the size of space between two appropriate neighbouring addresses required by different align
    code. For the reason that only six of eight situation is used, there could be big limitation and small limitation. Statistics shows 
    that TIBs whose align code is <i>AE_FALLBACK</i> and <i>AE_PATTERN_0x7</i> have the bigger size and amount, I distribute them bigger
    size limitation.
</p>
<table>
<tr>
<td align="center"><img src="Holes.JPG" alt="Holes"></td>
</tr>
<tr>
<td align="center"><b>Figure 4. </b>Use proper TIB to fill the holes before</td>
</tr>
</table> 
</li>
</ul>
<h2>Result</h2>
<p> In order to measure the performance of the allocator after modification, I generalize 3 bits to 8 bits for encoding.
    The other immutable states have not be dicided, but I have to make the distinction of different align code big enough. 
    Therefore, I use the first 3 bits of 8 bits for GC metadata instead of the last 3 bits, and other bits will be set to zero
    all the time.</p>
<p> What I use to measure at the run time is <a href="http://dacapobench.org/">DaCapo 2006 benchmark</a>, which is 
    is intended as a tool for Java benchmarking by the programming language, memory management and computer architecture communities.
    I choose <i>fop</i> , <i>xalan</i>, <i>pmd</i> , <i>pmd</i> , <i>bloat</i>, <i>eclipse</i> and <i>antlr</i> benchmark as the main
    example.
</p>

<ul>
<li><h3>The distribution of TIB</h3>
<table>
<tr>
<td align="center"><img src="Number.JPG" alt="Number"></td>
</tr>
<tr>
<td align="center"><b>Figure 5. </b>The quantity distribution of different align value TIB</td>
</tr>
</table> 
<p> As mentioned before, the align value <i>AE_PATTERN_0x3D(0)</i> and <i>AE_PATTERN_0x3(4)</i> is never used. This can be comfirmd by
    the relevant code. In the figures after, I will not show these two value again. And the align value <i>AE_FALLBACK(7)</i> is the most frequent.
</p>
<table>
<tr>
<td align="center"><img src="Mean.JPG" alt="Mean"></td>
</tr>
<tr>
<td align="center"><b>Figure 6. </b>Mean size distribution of different align value TIB</td>
</tr>
</table> 
<p> According to data analysis, the TIBs whose align value is <i>AE_FALLBACK(7)</i>, <i>AE_PATTERN_0x3F(6)</i> and <i>AE_PATTERN_0x7(3)</i> 
    have larger average size. For <i>AE_FALLBACK(7)</i> and <i>AE_PATTERN_0x7(3)</i> have larger amount, I assign them bigger
    size limitation at run time (Align value 0,4 never be involved).
</p>
</li>
<li><h3>The effect of optimization</h3>
<table>
<tr>
<td align="center"><img src="Size.JPG" alt="Size"></td>
</tr>
<tr>
<td align="center"><b>Figure 7. </b>Total Size of TIB Space Under Different Allocation Strategy at run time (Fill the holes when already removed the tail)</td>
</tr>
</table>
<p> The result shows that 3 bits to encode is cost-efficient, the additional cost is insignificant compared with 
    the size of TIB itself even though the former allocator is not that intelligent. The space cost of 8 bits 
    is much more, however, the effect of optimization is remarkable. As shown in the <i>Figure 8</i>, no matter which
    benchmark, more than half of the waste space is reduced when two strategies work together. The total size after the optimization is acceptable, 
    it is worthwhile spending these space to obtain more bits for encoding.
</p>
<table>
<tr>
<td align="center"><img src="Waste.JPG" alt="Waste"></td>
</tr>
<tr>
<td align="center"><b>Figure 8. </b>Size of Waste Space Under Different Allocation Strategy at run time(Fill the holes when already removed the tail)</td>
</tr>
</table>
<p> Moreover, the time to maintain several lists is negligible. Although the time it takes to pass the benchmark is different 
    from time to time, the average time is almost unchanged. As for build time, it may spends some time to arrange the address of
    all 2034 TIBs, while the importance of time use in build time is not as much as that of run time, it is built only once.
    The effect of global optimization is also significant, the total waste reduced by 3/4, which ensures it impossible to 
    overstep the boundry of <i>BOOT_IMAGE_DATA</i>.
</p>
<table>
<tr>
<td align="center"><img src="Build.JPG" alt="Build"></td>
</tr>
<tr>
<td align="center"><b>Figure 9. </b>Size of Waste Space Under Different Allocation Strategy at build time(Greedy algorithm work with 'remove the tail')</td>
</tr>
</table>
</li>
</ul>

<h2>To be improved</h2>
<p> For the reason that what immutable states will be used have not been decided, the bits other than the former
    three bits are set to zero all the time. Therefore, there are only eight possible values, actually. To simplify 
    my work, I just judge whether the align code of a TIB is equal to any of these eight values in the conditional
    sentences, when checking which group is this TIB belogs to. Once other bits works, the judgement should be 
    be changed to what range the align value of a TIB is in, then classify this TIB to its group.
</p>
<p> In addition, the utilization rate of the holes can be improved. In the current strategy, every hole will be 
    divided into several pieces. The distribution of different align value of TIB determines some types will
    seldom be used, there will be much surplus in the corresponding lists , while some types can hardly be set
    in the piece of holes and always need to allocate a new space from free data area. Also, the size limitation
    of each type can be annoying when the number of encoding bits is not big enough, the use of other bits will make 
    the group of align value more complicated as well.
</p>
<p> The solution is: maybe we will not divide the holes, and use only one list to store the holes, if a TIB could be
    set in this holes and will not cover the space of existing TIBs, then set it here and check whether each of the rest two holes created
    by this TIB can theoretically hold other TIBs. If so, we can add it to the list again, if not, we can just leave it.
    In my opinion, this strategy will works better, especially when the number of bits is not big enough. The only question is that
    we need to go through all the holes in the list and check whether each of them is suitable, which may need some time if the size of list
    is large. This consumption need to be verified worthy or not.
</p>
<table>
<tr>
<td align="center"><img src="Improve.JPG" alt="Improve"></td>
</tr>
<tr>
<td align="center"><b>Figure 10. </b>Improve the utilization rate of the holes</td>
</tr>
</table>

<h2>Reference</h2>
[1]Robin Garner, Stephen M Blackburn, Daniel Frampton. A Comprehensive Evaluation of Object Scanning Techniques.
    In <i>ISMM '11 Proceedings of the international symposium on Memory management</i>, Pages 33-42, June 2011. 
